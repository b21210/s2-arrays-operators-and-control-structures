package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraysSample {
    public static void main(String[] args) {
//Arrays = fixed/limited collections of data.

// 2^31 = 2,147,483,648 elements.

//        Declaration
        int[] intArray = new int[3];

        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;

        System.out.println(intArray[2]);

//        other syntax used to declare arrays
        int intSample[] = new int[2];
        intSample[0] = 50;
        System.out.println(intSample[0]);

//        String array
        String[] stringArray = new String[3];
        stringArray[0] = "John";
        stringArray[1] = "Jane";
        stringArray[2] = "Mina";

        System.out.println(Arrays.toString(stringArray));

//        Declaration with initialization
        int[] intArray2 = {100, 200, 300, 400, 500};

        System.out.println(intArray2);

        System.out.println(Arrays.toString(intArray2));
//        ("[]")
        System.out.println(Arrays.toString(stringArray));

//        ====================================================
//        methods used in arrays
//        sort()
        Arrays.sort(stringArray);
        System.out.println(Arrays.toString(stringArray));

//        binary search
//         - searches the specified array of the given data type for the Arrays.sort() method prior ro making to making this call. If it is not sorted, the results are undefined
        String searchTerm = "Kelly";
        int binaryResult = Arrays.binarySearch(stringArray, searchTerm);
        System.out.println(binaryResult);

//        ====================================================
//        Multidimensional Array
        String[][] classroom = new String[3][3];

//        first row
        classroom[0][0] = "Dahyun";
        classroom[0][1] = "Chaeyoung";
        classroom[0][2] = "Nayeon";
//        second row
        classroom[1][0] = "Luffy";
        classroom[1][1] = "Zorro";
        classroom[1][2] = "Sanji";
//        third row
        classroom[2][0] = "Loid";
        classroom[2][1] = "Yor";
        classroom[2][2] = "Anya";

        System.out.println(Arrays.deepToString(classroom));

//        ====================================================
//        ArrayLists - resizable arrays, wherein elements can be added or removed whenever it is needed.

//        Declaration
        ArrayList<String> students = new ArrayList<>();

//        Adding elements
        students.add("John");
        students.add("Paul");
        System.out.println(students);

//        Access elements to an ArrayList
        System.out.println(students.get(0));

//        Update/changing to an ArrayList
        students.set(1, "George");
        System.out.println(students);

//        Removing elements
        students.remove(1);
        System.out.println(students);

//        Removing all elements in an ArrayList using the clear()
        students.clear();
        System.out.println(students);

//        Getting the number of elements in an ArrayList using size()
//        Get the length of an Array List
        System.out.println(students.size());

//        We can also declare and initialize values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("June", "Albert"));
        System.out.println(employees);

//        ====================================================
//        Hashmaps -> key: value pair
        HashMap<String, String> employeeRole = new HashMap<>();

//        Adding fields
        employeeRole.put("Captain", "Luffy");
        employeeRole.put("Doctor", "Chopper");
        employeeRole.put("Navigator", "Nami");

        System.out.println(employeeRole);

//        Retrieving field value
        System.out.println(employeeRole.get("Captain"));

//        Removing elements
        employeeRole.remove("Doctor");
        System.out.println(employeeRole);

//        Retrieving hashmap keys/fields
        System.out.println(employeeRole.keySet());
//        Retrieving hashmap values
        System.out.println(employeeRole.values());

//        with Integers as values
        HashMap<String, Integer> grades = new HashMap<>();

        grades.put("english", 89);
        grades.put("math", 93);
        System.out.println(grades);

//        Hashmap with Array Lists
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradesListA = new ArrayList<>(Arrays.asList(75, 80, 90));
        ArrayList<Integer> gradesListB = new ArrayList<>(Arrays.asList(89, 87, 85));

        subjectGrades.put("Joe", gradesListA);
        subjectGrades.put("Jane", gradesListB);

        System.out.println(subjectGrades);

//        Operators
//        1. Arithmetic -> +, -, *, /, %
//        2. Comparison -> >, <, >=, <=, ==, !=
//        3. Assignment -> =, +=, -=, *=, /=
//        4. Logical -> &&, ||, !

    }
}
