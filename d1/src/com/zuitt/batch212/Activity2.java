package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.HashMap;

public class Activity2 {
    public static void main(String[] args) {
        //        Array
        int[] arraynum = new int[5];
        arraynum[0] = 2;
        arraynum[1] = 3;
        arraynum[2] = 5;
        arraynum[3] = 7;
        arraynum[4] = 11;

        System.out.println("The first prime number is: " + arraynum[0]);

//        ArrayList
        ArrayList<String> list = new ArrayList<>();
        list.add("John");
        list.add("Jane");
        list.add("Chloe");
        list.add("Zoey");

        System.out.println("My friends are: " + list);

//        HashMaps
        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our inventory consists of: " + inventory);
    }
}
