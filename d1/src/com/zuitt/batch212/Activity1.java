package com.zuitt.batch212;

import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
//        Leap year
        int year;

        Scanner userInput = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year:");
        year = Integer.parseInt(userInput.next().trim());

        if(year % 4 == 0)
            System.out.println(year + " is a leap year");
        else if(year % 100 == 0)
            System.out.println(year + " is a leap year");
        else
            System.out.println(year + " is a NOT leap year");
    }
}
